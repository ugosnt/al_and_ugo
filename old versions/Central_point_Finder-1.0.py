# Central_point_Finder1_0.py
# version 0_18man fuse the resulting image with the chosen map with function blend
# version 0_19 can have in input any images (of any size)
# version 0_25 input also the name of the final image; faster crosses insertion
# version 0_26 correcte the distance in term 1 and term2: i left manhattan, now it is euclidean
# version 0_27 accepts variables as command line

# this is the version with the Euclidean distance

#  output files o more files contianing map of probability of the site of starting

# Authors Alessio Papini e Ugo Santosuosso, Department of Plant Biology University of Florence Italy, Via La Pira, 4 Firenze, mail alpapiniATunifi.it 

import os, sys
import math
#
import Image
#------- old defaults ---
#B=70 default value
#f=0.03 default value
#g=0.1 default value
#------------------------
B=1
f=0.03
g=0.8
print"The coordinates must be provided in a simple text file with x coordinates in the first line and y in the second lines. Values separated by one space(0,0 upper left corner of the image"
print "How the program works: insert parameters following the program name: that is python Killaleugo1_0.py nameimagemap.bmp coordinates.txt finalimage.bmp B(default 1) f(default 0.03) g(default 0.8)"
i2=sys.argv[1]
c1=sys.argv[2]
finim=sys.argv[3]
B=float(sys.argv[4])
f=float(sys.argv[5])
g=float(sys.argv[6])
#
c = open(c1, 'r')
# separation of first line to x coordinates and second line to y coordinates and transformation of string data in lists (of strings of the numbers)
import string
casesx=string.split(c.readline())
casesy=string.split(c.readline())
#check if x coordinates are equal to y coordinates and assignment of values to variable numbercases
numbercases = len(casesx)
numbercasesbis = len(casesy)
if numbercases != numbercasesbis:
 print('The number of x coordinates is different from the number of y coordinates!! Please check the data')
#transformation of the list of coordinates in list of integer values
case_x=[]
case_y=[]
for it in range (numbercases):
 case_x.append(int(casesx[it]))
 case_y.append(int(casesy[it]))
#
im2 = Image.open(i2)
WIDTH, HEIGHT = im2.size
i1=Image.new("RGB",(WIDTH,HEIGHT), "white")
i1.save("Imgwhite.bmp")
i1 = "Imgwhite.bmp"
i4 = "imagedef.bmp"
im1 = Image.open(i1)
# width and height for the two nested cycle needed to go through the image
im1.save(i4)
# create a image 
im4 = Image.open(i4)
#------------------------------------------------------
#  main cicle
#------------------------------------------------------
for i in range (WIDTH):
 for j in range (HEIGHT):
   result = 0
   for n in range (numbercases):
# euclidean distance 
#pitagora theorem: (math.pow((math.fabs(i - case_x[n])), 2) is the x catete; the other the y catete
     euc = math.sqrt(math.pow((math.fabs(i - case_x[n])), 2) + math.pow((math.fabs(j-case_y[n])),2))
#
     if euc > B:
#      
       term1 = 1 / math.pow(euc,f)
       result = result + term1
     else:
#
       term2=(math.pow(B,(g-f)))/math.pow((2*B-euc),g)
       result = result + term2
#
   if(i==0 and j==0):
     biggest_p = result;
     smallest_p = result;
   if(result > biggest_p):
     biggest_p = result; 
   if(smallest_p > result):
     smallest_p = result;

# hence i must use biggest_p and smallest_p to standardize the value of result: smallest_p=0; biggest_P=>255 
for i in range (WIDTH):
 for j in range (HEIGHT):
#i did a new cycle to give right colors otherwise i obtained a division by zero in the first cycle in the formula calculating resultrel = ((result-smallest_p)*255/(biggest_p-smallest_p))
   result = 0

   for n in range (numbercases):
# version euclidean distance 
     euc = math.sqrt(math.pow((math.fabs(i - case_x[n])), 2) + math.pow((math.fabs(j-case_y[n])),2))
     if (euc > B):                     
#
       term1 = 1 / math.pow(euc,f)
       result = result + term1
     else:
#      term2=(math.pow(B,(g-f)))/math.pow((2*B-math.fabs(i-case_x[n])-math.fabs(j-case_y[n])),g)
       term2=(math.pow(B,(g-f)))/math.pow((2*B-euc),g)
       result = result + term2
# (biggest_p-smallest_p)/result and biggest_p:255=result:x with x=resultrel
   resultrel = ((result-smallest_p)*255/(biggest_p-smallest_p))
#----------
# 
# second part to insert colors that is yellow for low probability and red for high probability
#
   fracres, intres = math.modf(resultrel) 
# the above is the integer part of result, that is intres that i will use underneath
# i must force the double variable to become integer: let's see how it is
   intintres = int(intres)
#
   a=0
   red=255
#
   if intintres <=243 and intintres>231:
    red=0
    intintres=0
    a=0  
   elif intintres <= 231: 
    a=intintres
   im4.putpixel((i,j),(red, 255-intintres, a))
# it puts a small cross on the cases coordinates
# so that only if the first condition is not satisfied (more common
# option) may be i have to insert a return before the successive elif but probably not
# successive step to produce a function that draws a cross in that position x,y 
   for cross in range (numbercases):
     im4.putpixel((case_x[cross],case_y[cross]),(0, 0, 0))
     if case_x[cross]<= (WIDTH-2): 
#if 3 spaces are free before the end of the line it puts 3 black dots on the right of the coordinate
#
      im4.putpixel((case_x[cross]+2,case_y[cross]),(0, 0, 0))
      im4.putpixel((case_x[cross]+1,case_y[cross]),(0, 0, 0))
     elif case_x[cross]<= (WIDTH-1): 
#if 2 spaces are free before the end of the line it puts 2 black dots on the right of the coordinate
#
      im4.putpixel((case_x[cross]+1,case_y[cross]),(0, 0, 0))
#
#from here the left part of the cross
     if case_x[cross]>= 2: 
#     im4.putpixel((case_x[cross]-3,case_y[cross]),(0, 0, 0))
      im4.putpixel((case_x[cross]-2,case_y[cross]),(0, 0, 0))
      im4.putpixel((case_x[cross]-1,case_y[cross]),(0, 0, 0))
     elif case_x[cross]>= 1:
#     im4.putpixel((case_x[cross]-2,case_y[cross]),(0, 0, 0))
      im4.putpixel((case_x[cross]-1,case_y[cross]),(0, 0, 0))
#    elif case_x[cross]!= 0: 
#     im4.putpixel((case_x[cross]-1,case_y[cross]),(0, 0, 0))

#from here up and down as above
     if case_y[cross]<= (HEIGHT-2):  
#upper part of the cross
      im4.putpixel((case_x[cross],case_y[cross]+2),(0, 0, 0))
      im4.putpixel((case_x[cross],case_y[cross]+1),(0, 0, 0))
     elif case_y[cross]<= (HEIGHT-1):  
      im4.putpixel((case_x[cross],case_y[cross]+1),(0, 0, 0))
#down part of the cross
     if case_y[cross]>= 2: 
      im4.putpixel((case_x[cross],case_y[cross]-2),(0, 0, 0))
      im4.putpixel((case_x[cross],case_y[cross]-1),(0, 0, 0))
     elif case_y[cross]>= 1:
#    if case_y[cross]!= 0: 
      im4.putpixel((case_x[cross],case_y[cross]-1),(0, 0, 0))
# with the above instruction it says that 255-result is not an integer but a double and i agree
# may be i should normalize tha value but in the c instruction ai don't see anything like that
#  im4.putpixel((i,j),(255, 255, 0)) if i use this instruction i obtain an image that is completely yellow
im4.save(i4)
im1=Image.open(i2)
im2=Image.open(i4)
im3=Image.blend(im1, im2, 0.7)
im3.save(finim)
#uso blend the value of the mask of 0.8 apparently works well,, It can be changed


print('Resulting image saved with the indicated name')



