PLT_IF_DATA_0.1.1a.py

#!/usr/bin/python2.7
#
# PLoT Isolation Forest anomalies from DATA v.0.1.1
# Ugo Santosuosso - 2018, July 2th 
#
# instructions for setting UTF-8 encoding as default encoding
#
# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
#
# import libraries
#
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest
import Image
import time
#
import argparse

####################-Functions Definition-#####################
def cases_to_array(casesx,casesy):
 #
 # converts cases coordinates lists in 2d vector
 #
 tuple_L=[]
 for i in range (len (casesx)):
    tuple=float(casesx[i]),float(casesy[i])
    tuple_L.append(tuple)
 myarray=np.asarray(tuple_L)
 return myarray


def randomize_data (X, n_samples_train):
 # X = data vector - it must be sampled and divided randomly between 
 # training set and test set.
 # n_samples_train = number of samples of the desired trainining set
 #
 # local variables
 #
 n_samples=len(X)
 percent = float(n_samples_train) / float(n_samples)
 rng = np.random.RandomState(RAND)
 cont_B = True
#
# repeat the original data randomization until the number of selected samples 
# it is equal to the desired one
#
 while (cont_B):
  cont=0

  X_train=[]
  X_test=[]
  for i in range(n_samples):
    #
    a = rng.rand()
    if a < percent :
        # add the selected data to the training list
        cont +=1
        X_train.append(X[i,:])
    else:
        # I add the selected data to the test list
        X_test.append(X[i,:])
  if int(cont)==int(n_samples_train):
    cont_B=False
    #
    # ----- convert lists to np.array -------
    #
    X_train=np.asarray(X_train)
    X_test=np.asarray(X_test)
    return X_train, X_test
 return X_train, X_test

###############

def Leggi_vettore (nome):
 import csv
 #
 # function for reading data and converting them to the right variable type
 #
 Sequential=[]
 casesx=[]
 casesy=[]
 weight=[]

 with open(nome, 'rb') as fcsv:
  reader = csv.reader(fcsv)
  for row in reader:
   num, a, b, c= row
   Sequential.append(int(num))
   casesx.append(float(a))
   casesy.append(float(b))
   weight.append(int(c))
#
# ----- converts list to array-------
#
 myarray=cases_to_array(casesx,casesy)
 
 return Sequential,myarray,weight
###############
def Grafica(X_train,X_test,xx,yy,Z,replica,n_repliche):
    #
    # levels number to display in "level bar"
    #
    num_level=10
    #
    plt.figure(replica)
    #
    plt.title("IsolationForest\n")
    plt.contourf(xx, yy, Z, num_level ,alpha=0.4,cmap=plt.cm.jet)
    plt.colorbar()
    #
    b1 = plt.scatter(X_train[:, 0], X_train[:, 1], c='xkcd:royal blue',label="New Train Observations")
    b2 = plt.scatter(X_test[:, 0], X_test[:, 1], c='xkcd:aqua green',label="New Regular Observations")
#
# marks the anomalous cases basing on the content of the "pred" vector : value "-1" = anomalous
#
    # tests if tere are not anomalies
    Anom1_bool=False
    Anom2_bool=False
    if -1  not in y_pred_train:
#        print "NO anomalies in c1"
        Anom1_bool=True
    else:
     for i in range (len(X_train)):
        if (y_pred_train[i]==-1):
        # comma after "c1" is mandatory
            c1,=plt.plot(X_train[i, 0], X_train[i, 1],"X",c='orange')
    if -1  not in y_pred_test:
#        print "No anomalies\ in c2"
        Anom2_bool=True
    else:
     for i in range (len(X_test)):
        if (y_pred_test[i]==-1):
        # comma after "c2" is mandatory
            c2,=plt.plot(X_test[i, 0], X_test[i, 1],"X",c='red')
#
# composes the legend if are not present anomalies in a data set
#
    if Anom1_bool == True and Anom2_bool==False :
        labels_handler=[b1, b2, c2]
        labels=["training observations", "new regular observations","anomalies in test"]
    if Anom1_bool == False and Anom2_bool==True :
        labels_handler=[b1, b2, c1]
        labels=["training observations", "new regular observations","anomalies in train"]
    if Anom1_bool == True and Anom2_bool==True :
        labels_handler=[b1, b2]
        labels=["training observations", "new regular observations"]
    if Anom1_bool == False and Anom2_bool==False :
        labels=["training observations", "new regular observations","anomalies in train","anomalies in test"]
        labels_handler=[b1, b2, c1,c2]
    #
    
    plt.legend(labels_handler,labels, loc="upper left")

# imposes the size of the graph window
# the second dimension must be reversed so that
# the representation of the images is straight

    plt.xlim((xx.min(),xx.max()))
    plt.ylim((yy.min(),yy.max()))
    
    if replica == (n_repliche-1):
        plt.show()
    return
###############
def Grafica_Compl(SeQ_train,X_train,W_train,SeQ_test,X_test,W_test,Range_X,Range_Y):
    #
    # Generates the overall figure of the anomalies detected by the method 
    #
    plt.figure()
    #
    plt.title("IsolationForest\n")
    #
    Alpha=0.7
    Scala=30
    Area1=[]
    Area2=[]
    #
    for i in range(len(X_train)):   
        Area1.append(Scala*W_train[i])
        plt.annotate(SeQ_train[i],(X_train[i,:]))
    b1 = plt.scatter(X_train[:, 0], X_train[:, 1], s=Area1 ,c='red', alpha=Alpha,marker='H')
    #
    #
    for i in range(len(X_test)):
        Area2.append(Scala*W_test[i])
        plt.annotate(SeQ_test[i],(X_test[i,:]))
    b2 = plt.scatter(X_test[:, 0], X_test[:, 1], s=Area2 ,c='blue', alpha=Alpha, marker='^')
    #
    labels=["Anomalies in Train Set","Anomalies in Test Set"]
    labels_handler=[b1,b2]
    #
    plt.xlim(Range_X)
    plt.ylim(Range_Y)
    plt.legend(labels_handler,labels, loc="best")
    plt.show()
    return

#####################################################################

#-----####------
# GLOBAL VARIABLES
#-----####------

nome_dati_osservazioni="dat.csv"

anomalie_train=[]
anomalie_test=[]
anomalie_train_set={}
anomalie_test_set={}
Sequential=[]
#
# ------------------------------------
#        ARGPARSE: reads command line PARAMETERS
# ------------------------------------
#
parser = argparse.ArgumentParser(description="Execute a set of Isolation Forest analysis on a dataset.")
parser.add_argument("-f","--train_fraction",type=float, default=0.25, help= "Data Set fraction to be used for training (0.0 - 1.0) - default: 0.25" )
parser.add_argument("-r","--replicas",type=int, default=10, help= "Number of replicas to be performed- default: 10" )
parser.add_argument("-d","--data",required=True, help= "Data file name in csv comma delimited format. "\
                                                             "The coordinates must be provided in a simple text file in csv format. " \
                                                             "Values on a row separated by a comma, " \
                                                             "First value in row is the sequential number of event, "\
                                                             "Second value in row is X value "\
                                                             "Third value in row is Y value "\
                                                             "Fourth value in row is weight (integer) value ( 0<weight<=100 ) "\
                                                             "Origin of the data (0,0) is upper left corner of the image")
args=parser.parse_args()
# ------------------------------------
#         Read the observations dataset.
# ------------------------------------
#
nome_dati_osservazioni=args.data
_,nome,ext=nome_dati_osservazioni.split(".")
Sequential,X_dat,_ = Leggi_vettore (nome_dati_osservazioni)
n_samples=len(X_dat)
nome_out="."+nome+"_RES."+ext
nome_out_train="."+nome+"_train_RES."+ext
nome_out_test="."+nome+"_test_RES."+ext
#
# identify the train samples
#
train_fraction = args.train_fraction
n_samples_train =int (n_samples*train_fraction)
n_samples_test = n_samples - n_samples_train
#
# variables to intercept data set - I suppose they are all positive
#
scala=0.25
min_x_range=X_dat[:,0].min() * ( 1.0 - scala )
min_y_range=X_dat[:,1].min() * ( 1.0 - scala )
max_x_range=X_dat[:,0].max() * ( 1.0 + scala )
max_y_range=X_dat[:,1].max() * ( 1.0 + scala )
Range_X=(min_x_range,max_x_range)
Range_Y=(min_y_range,max_y_range)

#
# sets the number of replicas for random repetition
#

n_repliche=args.replicas

#-----------------------------------
#--- Main Cicle Start            ---
#-----------------------------------
for replica in range (n_repliche):
 print "Computing replica n.:",replica
#
# initializes the random number generator -
#
 RAND=int(time.time())
 rng = np.random.RandomState(RAND)
#
# Generate train data
#
 X_train,X_test = randomize_data(X_dat,n_samples_train)
# -----------------------------------
# select parameters and fit the model
#
 clf = IsolationForest(max_samples=n_samples_train, random_state=rng)
 clf.fit(X_train)
#
# predict anomalies on train and test sets
#
 y_pred_train = clf.predict(X_train)
 y_pred_test = clf.predict(X_test)
#
# plot the line, the samples, and the nearest vectors to the plane
#
 xx, yy = np.meshgrid(np.linspace(min_x_range, max_x_range, int(n_samples/2)), np.linspace(min_y_range, max_y_range, int(n_samples/2))) #type (xx,yy): numpy.ndarray
#
# concatenate xx and yy vectors and create the decision functions
#
# 
 Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
 Z = Z.reshape(xx.shape) #type (Z): numpy.ndarray
#
 Grafica(X_train,X_test,xx,yy,Z,replica,n_repliche)

#
# finds anomalies and put them into the lists.
#
 for i in range(len(X_train)):
    if y_pred_train[i]==-1:
        anomalie_train.append((X_train[i,0],X_train[i,1]))

 for i in range(len(X_test)):
    if y_pred_test[i]==-1:
        anomalie_test.append((X_test[i,0],X_test[i,1]))

# ---------------------------------
# --- Main Cicle end            ---
# ---------------------------------
#
# converts the lists of anomalies found in each replica into a set so that duplicates are eliminated
#
anomalie_train_set=set(anomalie_train)
anomalie_test_set=set(anomalie_test)
#
# calculates how many times each anomaly appears within all replicas
#
print "In train set found n: ",len(anomalie_train_set)," anomalies with, in total, ", len(anomalie_train)," duplicates"
print "In test set found n: ",len(anomalie_test_set)," anomalies with, in total,", len(anomalie_test)," duplicates"

hist_train=[]
hist_test=[]

ats=list(anomalie_train_set)
pos=0
#
# generates the histogram of the appearances of the single anomalies
# for the training set 
#
for i in range (len(ats)):
    summ=0
    for j in range (len(anomalie_train)):
        if (ats[i] == anomalie_train[j]) :
            summ += 1
    for j in range (len(X_dat)):
        if ats[i] == (X_dat[j,0],X_dat[j,1]):
            #
            pos=Sequential[j]
    hist_train.append((pos,ats[i],summ))
    
  
ats1=list(anomalie_test_set)
#
# ......and for the testing set
#
for i in range (len(ats1)):
    summ=0
    for j in range (len(anomalie_test)):
        if (ats1[i] == anomalie_test[j]) :
            summ += 1
    for j in range (len(X_dat)):
        if ats1[i] == (X_dat[j,0],X_dat[j,1]):
            #
            pos=Sequential[j]
    hist_test.append((pos,ats1[i],summ))

print
#
# write results in a file
#
salva=open(nome_out,"w")
salva1=open(nome_out_train,"w")
stringa= "------ Train Set Results ------\n"
salva.write(stringa)
for i in range (len(hist_train)):
    pos,(a,b),c= hist_train[i]
    stringa=str(pos)+","+str(a)+","+str(b)+","+str(c)+"\n"
    salva.write(stringa)
    salva1.write(stringa)
salva1.close()    
stringa="-------- Test Set Results -------\n"
salva.write(stringa)
salva1=open(nome_out_test,"w")
for i in range (len(hist_test)):
    pos,(a,b),c= hist_test[i]
    stringa=str(pos)+","+str(a)+","+str(b)+","+str(c)+"\n"
    salva.write(stringa)
    salva1.write(stringa)
salva1.close()
salva.close()
#-------------------------------
# makes the overall graph of the anomalies
#-------------------------------
SeQ_train=[]
SeQ_test=[]
Train=[]
Test=[]
W_train=[]
W_test=[]
Range_X=(0,WIDTH)
Range_Y=(0,HEIGHT)
SeQ_train,Train,W_train=Leggi_vettore(nome_out_train)
SeQ_test,Test,W_test=Leggi_vettore(nome_out_test)
Grafica_Compl(SeQ_train,Train,W_train,SeQ_test,Test,W_test,Range_X,Range_Y)

print "END of execution."
## END
