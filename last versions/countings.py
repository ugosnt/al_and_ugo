#-------------- init --------------------
import os, sys
import Image
import math
Nero=0
Rosso=0
Giallo=0
Verde=0
Perc_Nero= 0.0
Perc_Rosso= 0.0
Perc_Giallo= 0.0
Perc_Verde= 0.0
#--------------------------------------
nome=sys.argv[1]
image_in = Image.open(nome)
WIDTH, HEIGHT = image_in.size
Area=WIDTH*HEIGHT
image_in = Image.open(nome)
for i in range (WIDTH):
 for j in range (HEIGHT):
  r,g,b = image_in.getpixel((i,j))
  if (r==0 and g==0 and b==0):
    Nero=Nero+1
  if (( r < 12 ) and ( g > 128 ) ):
    Verde=Verde+1
  if (( r > 12  ) and ( g > 99 )):
     Giallo=Giallo+1
  if (( r > 12  ) and ( g < 100 )):
    Rosso=Rosso+1
#
#
Perc_Nero=float( Nero )/ float( Area ) * 100.0
Perc_Rosso= float( Rosso) / float( Area ) *100.0
Perc_Giallo= float( Giallo) / float(Area ) *100.0
Perc_Verde= float ( Verde ) / float ( Area )*100.0
Totale=Nero+Verde+Giallo+Rosso
#
#
print " "
print "-------- ", nome , " ----------"
print " "
print " Dimensioni: ",WIDTH," * ",HEIGHT, " - Area: ", Area
print " "
print " Pixel Rossi : ", Rosso, " ( ",'%.4f' % Perc_Rosso, "% )"
print " Pixel Gialli: ", Giallo," ( ",'%.4f' % Perc_Giallo, "% )"
print " Pixel Verdi : ", Verde, " ( ",'%.4f' % Perc_Verde, "% )"
print " Pixel Neri  : ", Nero , " ( ",'%.4f' % Perc_Nero, "% )"
print " "
print " Totale pixel contati:", Totale
print " "
print "-------------------------------"
print ""
