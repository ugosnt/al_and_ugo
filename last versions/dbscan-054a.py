#
# Ugo Santosuosso - v.0.5.4alpha - 2-Oct-2017
#
# codes for UTF-8 data encoding
#
# encoding:utf-8
import os, sys
print "Loading systyem with encoding:", sys.getdefaultencoding()
reload(sys)
sys.setdefaultencoding('utf8')
print "Reloading systyem with encoding:",sys.getdefaultencoding()
# disble warnings for compabtibility problems with new python libraries 
import warnings
warnings.simplefilter("ignore")
#
import csv
import time
import argparse
import Image
#
import numpy as np
from numpy import *

#
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
#
#
# leggo i dati dal file - sostituisce la sezione precedente
#
# ----- variables init  -------
file_dati="temp.csv"
EPS=300.0
MIN_SAMPLE=5
METRIC="euclidean"
ALGO="auto"
HEIGHT=0
WIDTH=0
Image_Name=""
#
# ----- "argparse" ------
#
# read the command line arguments
#
parser = argparse.ArgumentParser(description="Performs DBSCAN on given dataset")

parser.add_argument("-E","--eps",type=float, default=300.0, help= " The maximum distance between two samples for them to be considered as in the same neighborhood. Default=300" )
parser.add_argument("-m","--min_sample",type=int, default=5, help= "The number of samples in a neighborhood for a point to be considered as a core point. Default=5" )
parser.add_argument("-A","--algorithm", default="auto", help= " avaiable algorithms: 'auto', 'ball_tree', 'kd_tree', 'brute' " \
                    "Default = auto")
parser.add_argument("-M","--metric",default="euclidean", help=" The metric to use when calculating distance between instances. "\
                    "'cityblock', 'cosine', 'euclidean', 'l1', 'l2', 'manhattan'  'braycurtis', 'canberra', 'chebyshev'," \
                    "'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski', 'mahalanobis', 'matching', 'minkowski'," \
                    "'rogerstanimoto', 'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule'."\
                    "Default = EUCLIDEAN ")
parser.add_argument("-B","--background",help="background image")
parser.add_argument("-d","--data",required=True, help= "Data file name in csv comma delimited format. ")

args = parser.parse_args()
#
###########################################################
#
file_dati=args.data
EPS=args.eps
MIN_SAMPLE=args.min_sample
ALGO=args.algorithm
#HEIGHT=args.H
#WIDTH=args.W
Image_Name=args.background
#
print file_dati,EPS, MIN_SAMPLE, METRIC, ALGO,Image_Name
c = open(file_dati, 'r')
#
# Create the lists of the coordinates of crimes from a csv file
#
Sequential=[]
casesx=[]
casesy=[]
weight=[]
#
# read the csv file
#
with open(file_dati, 'rb') as fcsv:
  reader = csv.reader(fcsv)
  for row in reader:
   num, a, b, c= row
   Sequential.append(num)
   casesx.append(a)
   casesy.append(b)
   weight.append(c)
NUM_CASES=len(casesx)
X=np.array(zeros((NUM_CASES,2)))

#
# controllare che HEIGHT e WIDTH non siano zero. in caso metterli come max dei valori dei dati del file moltiplicati per 1.5
#
if Image_Name != "":
    im2 = Image.open(Image_Name)
    WIDTH, HEIGHT = im2.size
else:
    WIDTH=casesx.max()*1.5
    HEIGHT=casesy.max()*1.5
######################
#
#memorizzo i dati nel vettore
#
for i in range (NUM_CASES):
 X[i][0]=int(casesx[i])
 X[i][1]=int(casesy[i])
##############################################################################
#
# Compute DBSCAN
#
db = DBSCAN(eps=EPS,min_samples=MIN_SAMPLE,metric=METRIC,algorithm=ALGO).fit(X)
core_samples_mask =np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.4f" % metrics.homogeneity_score(Sequential, labels))
print("Completeness: %0.4f" % metrics.completeness_score(Sequential, labels))
print("V-measure: %0.4f" % metrics.v_measure_score(Sequential, labels))
print("Adjusted Rand Index: %0.4f" % metrics.adjusted_rand_score(Sequential, labels))
print("Adjusted Mutual Information: %0.4f" % metrics.adjusted_mutual_info_score(Sequential, labels))
#
#  one cluster workaround for silhouette
#
if n_clusters_ == 1 :
    print ("Silhouette Coefficient: Undefined for a single cluster")
else:
    my_sil_val=metrics.silhouette_score(X, labels)
    print("Silhouette Coefficient: %0.3f" % my_sil_val)

##############################################################################
#
# Plot results
#

# Black removed and is used for noise instead.
unique_labels = set(labels)

print "** Writing raw data **"

# salva su file

for j in range (n_clusters_):
 print "\ncluster n.:", j 
 nome_out=str(file_dati)+"_clust_"+str(j)+".csv"
 salva=open(nome_out,"w")
 print "saved in : "+ nome_out +"\n"
 for i in range (NUM_CASES):
  if labels[i]==j :  
   a=str(i)+","+str(int(X[i,0]))+","+str(int(X[i,1]))+","+str(labels[i])+"\n" 
   salva.write(a)
   #print i, int(X[i,0]),int(X[i,1]),labels[i]
   #print a
 salva.close()
#
#
#
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))

for k, col in zip(unique_labels, colors):
    my_marker="o"
    if k == -1:
        # Black used for noise.
        col = 'k'
        my_marker="*"

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], my_marker, markerfacecolor=col, markeredgecolor='k', markersize=14)

    xy = X[class_member_mask & ~core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], my_marker, markerfacecolor=col, markeredgecolor='k', markersize=6)

plt.axis([0,WIDTH,HEIGHT,0])
#Title_String=" Estimated number of clusters: " + str(n_clusters_) +"\n"+" Silhouette Score: ",str(my_sil_val)
#plt.title(Title_String)
plt.title('Estimated number of clusters: %d \n ' % n_clusters_)

plt.imshow(im2)
plt.show()
#
# End
#