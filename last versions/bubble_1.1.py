print(__doc__)
"""
"""

import os, sys
import csv

import numpy as np

import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler

#Variabili  Globali
NUM_CLUST=10
NUM_POINT_X_CLUST=50
N_SAMPLES=NUM_CLUST*NUM_POINT_X_CLUST
BASE_NAME="out.csv"
# ampiezza dello spazio di simulazione
AMPLITUDE=512
CLUST_STD=1.1*(AMPLITUDE/32)
#print sys.argv
#
if ( len(sys.argv) > 2 ):
   NUM_CLUST=int(sys.argv[1])
   NUM_POINT_X_CLUST=int(sys.argv[2])
   CLUST_STD=float(sys.argv[3])*(AMPLITUDE/32)
   BASE_NAME=str(sys.argv[4])
else:
    print "usage: "+sys.argv[0]+" Num_CLusters Points_X_Cluster Standard_Dev Base_Name_Outputfile.csv"
    print " "
    print "   Defaults:"
    print "             Num_CLusters     = ", NUM_CLUST
    print "             Points_X_Cluster = ", NUM_POINT_X_CLUST
    print "             Standard_Dev     = 1.1"
    print "             Output File      = ", str(NUM_CLUST)+"_clust_"+BASE_NAME
    print " "
#
# Generate random centers of bubbles
#
N_SAMPLES=NUM_CLUST*NUM_POINT_X_CLUST
CENTERS=np.random.randint(low=(AMPLITUDE*0.2), high=(AMPLITUDE*0.8), size=(NUM_CLUST,2))
#
# calculates mean of (nclust-1) center and assigns it to the center of last cluster
#
center_x,center_y=np.mean(CENTERS[0:(NUM_CLUST-2)],axis=0)
CENTERS[(NUM_CLUST-1),0]=int(center_x)
CENTERS[(NUM_CLUST-1),1]=int(center_y)


##############################################################################
# Generate sample data
X, labels_true = make_blobs(n_samples=N_SAMPLES, centers=CENTERS ,cluster_std=(CLUST_STD), shuffle=True, random_state=None)
GLOBAL_MEAN=np.mean(CENTERS)
GLOBAL_STD=np.std(CENTERS)
print "       Center Mean and Standard Dev = ",GLOBAL_MEAN,GLOBAL_STD
print "       Global Standard Dev = ",(np.std(X)/(AMPLITUDE/32))

##############################################################################

# Plot result
fig1 = plt.gcf()
labels=labels_true
unique_labels = set(labels)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
#plot bubbles
for k in range (len(X)):
    plt.scatter(X[k][0], X[k][1], c=colors[labels[k]], marker='o')
plt.title('Number of clusters: %d' % NUM_CLUST)
#plot bubbles center
for k in range (NUM_CLUST):
    plt.scatter(CENTERS[k][0], CENTERS[k][1], c='b', marker='o')
# plots center of all cluster
plt.scatter(int(center_x),int(center_y),c='r',marker='x',s=500)
plt.axis([0,AMPLITUDE,AMPLITUDE,0])
plt.show()
plt.draw()
nome_fig="fig_"+BASE_NAME+"_"+str(NUM_CLUST)+"_clust.png"
fig1.savefig(nome_fig, dpi=100)
# Write all data in a csv file
# with format seq,x,y,weight
nome="STAT_"+BASE_NAME
salva=open(nome,"w")
salva.write("Num_CLusters         = "+str(NUM_CLUST)+"\n")
salva.write("Points_X_Cluster     = "+str(NUM_POINT_X_CLUST)+"\n")
salva.write("Standard_Dev         = 1.1"+"\n")
salva.write("Output File Basename = "+str(BASE_NAME)+"\n")
salva.write("Global Standard Dev  = "+str((np.std(X)/(AMPLITUDE/32)))+"\n")
salva.write("Center Mean and Standard Dev = "+str(GLOBAL_MEAN)+" "+str(GLOBAL_STD)+"\n")
salva.close()
# Write all data in a csv file
# with format seq,x,y,weight
#BASE_NAME=str(NUM_CLUST)+"_clust_"+BASE_NAME
nome=BASE_NAME
salva=open(nome,"w")
for i in range (N_SAMPLES):
  salva.write(str(i+1)+","+str(int(X[i,0]))+","+str(int(X[i,1]))+",100\n")
salva.close()
# Write centroids data in a csv file
# with format x,y
nome="centroids"+str(NUM_CLUST)+"cluster"+BASE_NAME+".txt"
salva=open(nome,"w")
for i in range (NUM_CLUST):
  salva.write(str(int(CENTERS[i,0]))+","+str(int(CENTERS[i,1]))+"\n")
salva.close()
# Write cluster data in a csv file one for each cluster
# with format seq,x,y,weight
for i in range (NUM_CLUST):
 nome="cluster"+str(i+1)+"di"+str(NUM_CLUST)+str(BASE_NAME)+".txt"
 print nome
 salva=open(nome,"w")
 for j in range (N_SAMPLES):
  if (labels[j]== i):
   # scrivo i risultati nel file...
   salva.write(str(j)+","+str(int(X[j,0]))+","+str(int(X[j,1]))+",100\n")
   #endif
 #endfor
 salva.close()
#ENDFOR
#
