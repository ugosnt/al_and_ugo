#
#
#
import os, sys
import math
import csv
import Image
import ImageDraw
import ImageFont
from numpy import *
# 
# Default parameters
#
rosso=(255, 0, 0)
giallo=(255, 255, 0)
blu=(0,0,255)
grigio=(128, 128, 128)
verde=(0, 255, 0)
nome=sys.argv[0]
#
NUM_ARGS=3
#
#
if (len(sys.argv) < NUM_ARGS ):
#
 print" ------------------------------- "
 print" "
 print" ------ "+nome+" --------"
 print" "
 print" Usage: python ",nome," image_name data_name.csv"
 print" "
 print" ------------------------------- "
 sys.exit (0)
#
# Retrives the parameters for the reconstruction from the command line
#
# i2 - name of the image containing the map where the coordinates have been calculated, 
#  with its path to directory if different from the current working directory
#
i2=sys.argv[1]
c1=sys.argv[2]
c = open(c1, 'r')
im2 = Image.open(i2)
WIDTH, HEIGHT = im2.size
c2 = i2+"mix"+c1+".bmp"
print i2, c1
#
# Create the lists of the coordinates of crimes from a csv file
#
Sequential=[]
crimesx=[]
crimesy=[]
weight=[]
testo=" "
lung_testo=0
#
# read the csv file
#
with open(c1, 'rb') as fcsv:
  reader = csv.reader(fcsv)
  for row in reader:
   num, a, b, c= row
   Sequential.append(num)
   crimesx.append(a)
   crimesy.append(b)
   weight.append(c)
#
NB_CRIMES=len(crimesx)
#
Sequenza=[]
crime_x=[]
crime_y=[]
pesi=[]
#
# fill the lists of crimes coordinates
#
for it in range (NB_CRIMES):
  crime_x.append(float(crimesx[it]))
  crime_y.append(float(crimesy[it]))
  pesi.append((float(weight[it])/100))  
#
print" Drawing points.. ",
draw = ImageDraw.Draw(im2)
for cross in range (NB_CRIMES):
  draw.ellipse((crime_x[cross]-2, crime_y[cross]-2, crime_x[cross]+2, crime_y[cross]+2), fill=(255,0,0))
  testo="n."+str(Sequential[cross])
  lung_testo=len(testo)
  print testo, " ** ", lung_testo
  draw.text((crime_x[cross]-int(5*lung_testo/2), crime_y[cross]-13),testo,fill=(0,0,255))

im2.save(c2)

