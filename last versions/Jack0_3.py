#!/usr/bin/python
#
#---------------------------------------------------
#
# JackKnife v.0.2 - 30 / 01 / 2013
# 
# Authors:
# Alessio Papini - Department of Plant Biology University of Florence Italy, Via La Pira, 4 Firenze, mail alpapiniATunifi.it
# Ugo Santosuosso - Department of Anatomny, Istology and Forensic Medicine, Largo Brambilla, 1 Firenze, mail ugoATunifi.it
# 
# Implements:
#
#---------------------------------------------------
#
# importo le librerie 
#
#
import os, sys
import math
import numpy
import csv
from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq,whiten
from numpy import *
#
#
NUM_ARGS=3
#
# recupero gli argomenti da linea di comando
#
nome=sys.argv[0]
i2csv=sys.argv[1]
n_repliche=int(sys.argv[2])
n_elem_tolti=int(sys.argv[3])
#
if (len(sys.argv) < NUM_ARGS ):
#
 print" ------------------------------- "
 print" "
 print" ------ "+nome+" --------"
 print" " 
 print" The coordinates must be provided in a simple text file in csv format,"
 print" with x coordinates in the first column and y in the second column."
 print" Values on a row separated by a comma (0,0) is upper left corner of the image"
 print" How the program works: insert parameters following the program name: that is "
 print" "
 print" python "+nome+" coordinates.csv numb3rs_of_replica numb3rs_of_elements_left"
 print" "
 print" No path specifications = current directory"
 print" "
 print" ------------------------------- "
 sys.exit (0)
#
# qui sotto va rivisto per eliminare le ridondanze
#
crimescsvx=[]
crimescsvy=[]
pesi=[]
Num=[]
with open(i2csv, 'rb') as fcsv:
  reader = csv.reader(fcsv)
  for row in reader:
   num, acsv, bcsv, peso= row
   Num.append(num)
   crimescsvx.append(acsv)
   crimescsvy.append(bcsv)
   pesi.append(peso)
   # end for row ---
# end with ---
NB_CRIMEScsv=len(crimescsvx)
NB_CRIMEScsvbis = len(crimescsvy)
if NB_CRIMEScsv != NB_CRIMEScsvbis:
 print('The number of x coordinates is different from the number of y coordinates!! Please check the data')
#
# assegnazioni dei vettori e delle liste
#
allcoord=zeros( (NB_CRIMEScsv,2) )
nnum=zeros(NB_CRIMEScsv)
ppes=zeros(NB_CRIMEScsv)
vettorex=[]
vettorey=[]
vettoren=[]
vettorep=[]
#
# riempio il vettore delle coordinate - secondo me questo passaggio sembra superfluo
#
for it in range (NB_CRIMEScsv):
  allcoord[it][0]=(int(crimescsvx[it]))
  allcoord[it][1]=(int(crimescsvy[it]))
  nnum[it]=(int(Num[it]))
  ppes[it]=(int(pesi[it]))
#
# end for it
#
for k in range(NB_CRIMEScsv):
  vettorex.append(allcoord[k,0])
  vettorey.append(allcoord[k,1])
  vettoren.append(nnum[it])
  vettorep.append(ppes[it])
#
# end for k ----
#
print "Generating replicas: ",
for i in range (n_repliche):
 #
 # Genera il nome del file
 #
 nome="replica"+str(i+1)+"di"+str(n_repliche)+"tolti"+str(n_elem_tolti)+"in"+i2csv+".txt"
 #
 # legge il vettore
 for k in range (NB_CRIMEScsv):
  vettorex[k]=allcoord[k,0]
  vettorey[k]=allcoord[k,1]
  vettoren[k]=Num[k]
  vettorep[k]=pesi[k]
  #
  # uscita a video di debug
  #
  # print vettoren[k],vettorex[k],vettorey[k],vettorep[k]
 #
 #end for k---
 #
 print i,".. ",
 for j in range (n_elem_tolti):
    # sceglie a caso un elemento del vettore dei dati
    fine=int(NB_CRIMEScsv-j-1)
    elemento_tolto=numpy.random.random_integers(0,fine)	 	
    vettorex.pop(elemento_tolto)
    vettorey.pop(elemento_tolto)
    vettorep.pop(elemento_tolto)
    vettoren.pop(elemento_tolto)
 # end for j ----------
 # 
 #scrive la replica - se il file non esiste lo crea altrimenti lo svuota 
 #
 salva=open(nome,"w")
 salva.close()
 #
 # scrive i dati in formato csv
 #
 salva=open(nome,"a")
 for k in range ( NB_CRIMEScsv - n_elem_tolti ):
   salva.write(str(int(vettoren[k]))+","+str(int(vettorex[k]))+","+str(int(vettorey[k]))+","+str(int(vettorep[k]))+"\n")
  #endfor i ---
 salva.close() 
 #
 # Sopra ho tolto "n_elem_tolti" dal vettore che ora e' piu' corto.
 # devo riaggiungere questi elementi altrimenti poi non riesce a rimettere
 # i valori del vettore letto
 #
 for k in range (n_elem_tolti):
     vettoren.insert(0,0)
     vettorex.insert(0,0)
     vettorey.insert(0,0)
     vettorep.insert(0,0)
 #
 # riassegno i valori del vettore letto
 #
 #endfor ---
print "done!"
#endfor ---


