# MixImage 0.9.1
# 
# Mix the images resulting from a JacKnifed Rossmo sequential reconstruction
#
# Authors Alessio Papini e Ugo Santosuosso, Department of Plant Biology University of Florence Italy, Via La Pira, 4 Firenze, mail alpapiniATunifi.it
#
# Usage examples
#  python miximage0.9.py ricostruzione 2 prova_or.bmp OR
#  python miximage0.9.py ricostruzione 2 prova_and.bmp AND
#  python miximage0.9.py ricostruzione 2 prova_mean.bmp MEAN
#
# dove: "ricostruzione" sta per la base del nome 
#        2 sta per il numero di repliche ( che possono essere a piacere )
#       "prova.bmp" sta per il file di uscita dove vengono mescolate le immagini
#        AND/OR/MEAN sono il tipo di operazione eseguita sui singoli pixel per generare il risultato(immagine) finale
#
import os, sys
import Image
import math
#
#
from numpy import *
#----------------
#
rosso=(255, 0, 0)
giallo=(255, 255, 0)
verde=(0, 255, 0)
blu=(0, 0, 255)
grigio=(128, 128, 128)
nero=(0, 0, 0)
colore=nero
colore_new=[]
#
#----------------
myimage=[]
nome_in=sys.argv[1]
repliche=int(sys.argv[2])
nome_out=sys.argv[3]
tipo=sys.argv[4]
for i in range (repliche):
  replica="replica"+str(i+1)+"di"+sys.argv[2]+sys.argv[1]+".bmp"
  print replica + " ",
  myimage.append(replica)
print 
#
# crea una immagine per di output
#
image_in = Image.open(myimage[0])
WIDTH, HEIGHT = image_in.size
image_out=Image.new("RGB",(WIDTH,HEIGHT), (0,0,0))
image_out.save(nome_out)
image_out = Image.open(nome_out)
#------------------------------------------------------
# OR
#------------------------------------------------------
if (tipo=="OR"):
#
# unisce le aree verdi
#
 print "Merging Green Zone...",
 for k in range (len(myimage)):
  image_in = Image.open(myimage[k])
  for i in range (WIDTH):
   for j in range (HEIGHT):
    colore = image_in.getpixel((i,j)) 
    if (( colore[0] < 12 ) and ( colore[1] > 128 ) ):
      image_out.putpixel((i,j),verde)
#
# unisce le aree gialle
#
 print " Yellow Zone...",
 for k in range (len(myimage)):
  image_in = Image.open(myimage[k])
  for i in range (WIDTH):
   for j in range (HEIGHT):
    colore = image_in.getpixel((i,j)) 
    if (( ( colore[0] > 128 ) and ( colore[1] > 128 ) )):
      image_out.putpixel((i,j),giallo)
#
# unisce le aree rosse
#
 print " Red Zone...",
 for k in range (len(myimage)):
  image_in = Image.open(myimage[k])
  for i in range (WIDTH):
   for j in range (HEIGHT):
    colore = image_in.getpixel((i,j)) 
    if ( (colore[0] > 12  ) and (colore[1]< 12)):
      image_out.putpixel((i,j),rosso)
# ---------------------------------------------------
# Media
# ---------------------------------------------------
if (tipo=="MEAN"):
 matrice=zeros((WIDTH,HEIGHT,3))
#
# per ogni immagine
#
 for k in range (len(myimage)):
 #
 # apro la immagine 
 #
  image_in = Image.open(myimage[k])
  for i in range (WIDTH):
   for j in range (HEIGHT):
    #
    # leggo il pixel
    #
    colore = image_in.getpixel((i,j))
    #
    # sommo le componenti di colore in matrice
    #
    matrice[i,j,0]=matrice[i,j,0]+colore[0]
    matrice[i,j,1]=matrice[i,j,1]+colore[1]
    matrice[i,j,2]=matrice[i,j,2]+colore[2]
 #
 # divido le componenti di colore per il numero di immagini ottenendo la immagine media
 #
 for i in range (WIDTH):
  for j in range (HEIGHT):
   for c in range (3):
    matrice[i,j,c]=int(matrice[i,j,c]/(len(myimage)))
 #
 # Rileggo la immagine salvando i colori ottenuti
 #
 for i in range (WIDTH):
  for j in range (HEIGHT):
   r,g,b=matrice[i,j]
   if (( r < 12 ) and ( g > 128 ) ):
      image_out.putpixel((i,j),(0,int(g),0))
   if (( r > 12  ) and ( g > 99 )):
      image_out.putpixel((i,j),(int(r),int(g),0))
   if (( r > 12  ) and ( g < 100 )):
      image_out.putpixel((i,j),(int(r),0,0))
# ---------------------------------------------------
# AND
# ---------------------------------------------------
if (tipo=="AND"):
 matrice=zeros((WIDTH,repliche,3))
 and_logico=zeros( (WIDTH,3) )
#
# per ogni riga
#
 for j in range (HEIGHT):
  #
  # per tutte le immagini
  #
  for k in range (repliche):
   #
   # apro una immagine e ne leggo una riga
   #
   image_in = Image.open(myimage[k])
   for i in range (WIDTH):
    colore = image_in.getpixel((i,j))
    #
    # memorizzo la riga della replica nella corrispondente riga della matrice
    #
    matrice[i,k,0]=colore[0]
    matrice[i,k,1]=colore[1]
    matrice[i,k,2]=colore[2]
    #if ( (colore[0]<12) and (colore[1]>128) ):
    #   matrice [i,k]=verde
    #if ( (colore[0]>12) and (colore[1]>99) ):
    #   matrice [i,k]=giallo
    #if ( (colore[0]>12) and (colore[1]<100) ):
    #   matrice [i,k]=rosso
  #
  # ora dovrei fare lo AND fra le righe
  #
  for i in range (WIDTH):
    #and_logico[i,0]=( matrice[i,repliche-1,0] and matrice[i,repliche-2,0] )
    #and_logico[i,1]=( matrice[i,repliche-1,1] and matrice[i,repliche-2,1] )
    #and_logico[i,2]=( matrice[i,repliche-1,2] and matrice[i,repliche-2,2] )
    if ( matrice[i,repliche-1,0] == matrice[i,repliche-2,0] ):
     if ( matrice[i,repliche-1,1] == matrice[i,repliche-2,1] ):
      if ( matrice[i,repliche-1,2] == matrice[i,repliche-2,2] ):
         and_logico[i]=matrice[i,repliche-1]    
      else:
        and_logico[i,2]=nero
    #----------------------------------------------------------------
    for k in range (repliche-2):
     if ( and_logico[i,0] == matrice[i,k,0] ):
      if ( and_logico[i,1] == matrice[i,k,1] ):
       if ( and_logico[i,2] == matrice[i,k,2] ):
         # and_logico[i]=matrice[i,k]    
         continue
       else:
        and_logico[i,2]=nero
    # and_logico[i,0]=( and_logico[i,0] and matrice[i,k,0] )
    # and_logico[i,1]=( and_logico[i,1] and matrice[i,k,1] )
    # and_logico[i,2]=( and_logico[i,2] and matrice[i,k,2] )
  #
  # scrivo il rigo della immagine
  #
  for i in  range (WIDTH):
   r,g,b=and_logico[i]
   and_logico[i]=(0,0,0)
#   image_out.putpixel((i,j),(int(r),int(g),0))
   image_out.putpixel((i,j),(int(r),int(g),int(b)))
#
#
# salvo la immagine risultante
#
print " Saving image"
image_out.save(nome_out)
print('Resulting image saved with the indicated name')


