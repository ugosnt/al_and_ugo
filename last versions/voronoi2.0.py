from PIL import Image
import random
import math
import os, sys
import numpy
import Image
import ImageDraw
import ImageFont
import csv
# 
# usage python voronoi*.py map.bmp num_clust dataset.csv 
#
#here the input: name of the image (in bmp format); number of cells; list of coordinates of the centroids
nx = []
ny = []
nr = []
ng = []
nb = []
#-----
crimesx=[]
crimesy=[]
weight=[]
crime_x=[]
crime_y=[]
pesi=[]
#
ima=sys.argv[1]
cells=int(sys.argv[2])
coord=sys.argv[3]
BLEND_PERCENTAGE=0.6
# try file centroids.txt: may be the number of cells can by assumed from the number of centroids; try midmed.bmp as image
# i have coordinates already splitted
#
nome="centroids"+str(cells)+"cluster"+coord+".txt"
print nome
#
centroid = open(nome, 'r')
#
#
#
im2 = Image.open(ima)
imgx, imgy = im2.size
image = Image.new("RGB", (imgx, imgy))
putpixel = image.putpixel
#----- legge i dati dei casi -------
with open(coord, 'rb') as fcsv:
  reader = csv.reader(fcsv)
  for row in reader:
    num, a, b, c= row
    crimesx.append(a)
    crimesy.append(b)
    weight.append(c)
#
NB_CRIMES=len(crimesx)
#
# fill the lists of crimes coordinates
#
for it in range (NB_CRIMES):
   crime_x.append(float(crimesx[it]))
   crime_y.append(float(crimesy[it]))
   pesi.append((float(weight[it])/100))
#----------------------
#----- legge i dati dei centroidi -------
with open(nome, 'rb') as fcsv:
    reader = csv.reader(fcsv)
    for row in reader:
     a, b = row
     nx.append(int(a))
     ny.append(int(b))
#----------------------
amp1 = 5
amp2 = 3
#----------------------
for i in range(cells):
     nr.append(random.randrange(256))
     ng.append(random.randrange(256))
     nb.append(random.randrange(256))
# may be here i should insert the coordinates? probably not
for y in range(imgy):
    for x in range(imgx):
        dmin = math.hypot(imgx-1, imgy-1)
        j = -1
        for i in range(cells):
            d = math.hypot(int(nx[i])-x, int(ny[i])-y)
            if d < dmin:
                dmin = d
                j = i
        putpixel((x, y), (nr[j], 0, nb[j]))
#
im3=Image.blend( image, im2, BLEND_PERCENTAGE )
draw = ImageDraw.Draw(im3)
# now insert circles at centroids coordinates 
for cross in range (cells):
     draw.ellipse((nx[cross]-amp1, ny[cross]-amp1, nx[cross]+amp1, ny[cross]+amp1), fill=(255,0,0))
for cross in range (NB_CRIMES):
     testo="n."+str(int(cross+1))
     lung_testo=len(testo)
     draw.ellipse((crime_x[cross]-amp2, crime_y[cross]-amp2, crime_x[cross]+amp2, crime_y[cross]+amp2), fill=(0,255,255))
     draw.text((crime_x[cross]-int(5*lung_testo/2), crime_y[cross]-13),testo,fill=(255,255,0))
nameimage="vorondiagblend_"+str(cells)+"_clust.png"    
im3.save(nameimage, "png")
nameimage="vorondiag_"+str(cells)+"_clust.png"    
image.save(nameimage, "png")