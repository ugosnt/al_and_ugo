#
#---------------------------------------------------
#
# Geo_Profiling v.2.0.3 - 6 / 02 / 2013
# Authors:
# Alessio Papini - Department of Plant Biology University of Florence Italy, Via La Pira, 4 Firenze, mail alpapiniATunifi.it
# Ugo Santosuosso - Department of Anatomny, Istology and Forensic Medicine, Largo Brambilla, 1 Firenze, mail ugoATunifi.it
# 
# Implements:
# - euclidean Powed (p=3) and Manhattan distance
# - cvs support
# - test input cases data are not to close to image border
# - test the correct arguments numbers
# - auto generate output file name
# - preserve elaboration data (usefull for blending images with clustering)
# - Euclidean and Manhattan distance
# - defaults parameters
# - New point rapresentation
# - points labelling
# - High visibility palette
# - weight for any point
# - r.o.i. bull eye rapresentation
#
#---------------------------------------------------
#
import os, sys
import math
#
# imports libraries for using Matrix, images and csv files
#
from numpy import *
import csv
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
# 
# Default parameters
#
rosso=(255, 0, 0)
giallo=(255, 255, 0)
blu=(0,0,255)
grigio=(128, 128, 128)
verde=(0, 255, 0)
#
NUM_ARGS=3
BLEND_PERCENTAGE=0.7
#
B=6
f=0.4
g=0.4
Dist="E"
Graph="Y"
#
# Retrives the name of the program from the command line
#
nome=sys.argv[0]
if (len(sys.argv) < NUM_ARGS ):
#
 print" ------------------------------- "
 print" "
 print" ------ "+nome+" --------"
 print" " 
 print" The coordinates must be provided in a simple text file in csv format."
 print" Values on a row separated by a comma"
 print"        First value in row is the sequential number of event"
 print"        Second value in row is X value"
 print"        Third value in row is Y value"
 print"        Fourth value in row is weight value ( 0<weight<=100 )"
 print" Origin of the data (0,0) is upper left corner of the image"
 print" How the program works: insert parameters following the program name: that is "
 print" "
 print" python "+nome+" nameimagemap.bmp coordinates.csv name_prefix_of_results_NO_EXT Dist Graph B f g "
 print" "
 print" REQUIRED:"
 print""
 print" * nameimagemap.bmp coordinates.csv name_prefix_of_results_NO_EXT *"
 print" "
 print"                  note: No path specifications = current directory"
 print" "
 print" Dist = type of distance E (euclidean-default) M (Manhattan) P (p-distance p=3)"
 print""  
 print" Graph - Y = full graphic output (default)"
 print"         B = only bull eye"
 print"         N = no label"
 print"         O = only label number"
 print""
 print" Suggested defaults are B = ",B
 print"                        f = ",f
 print"                        g = ",g
 print" "
 print" ------------------------------- "
 sys.exit (0)
#
# Retrives the parameters for the reconstruction from the command line
#
# i2 - name of the image containing the map where the coordinates have been calculated, 
#  with its path to directory if different from the current working directory
#
i2=sys.argv[1]
#
# c1 = raw_input('Write the name of the simple text file containing the coordinates of the cases:
# the first line containing the x coordinates and the second line containing the y coordinates
# (starting from left, up, as in GIMP images; the values must be separated by spaces)
#
c1=sys.argv[2]
#
# RES_PREFIX - the prefix name of the results with probability of finding the origin of the events
#
RES_PREFIX=sys.argv[3]
final_image=RES_PREFIX+".bmp"
#
# Command line parameters test
#
if (len(sys.argv) > (NUM_ARGS+1)):
# testfor fraph type
  Graph = sys.argv[4]
  if ( (Graph <>"Y") and (Graph <>"B") and (Graph <>"N") and (Graph <>"O")):
      Graph="Y" 
  # test for distance type
  if (sys.argv[5]<>"E"):
    Dist = sys.argv[5]
#
# test for parameters B f and g
# parameters required from the Rossmo Formula
#
if (len(sys.argv) > (NUM_ARGS+3)):
    B=float(sys.argv[6])
if (len(sys.argv) > (NUM_ARGS+4)):
    f=float(sys.argv[7])
if (len(sys.argv) > (NUM_ARGS+5)):
    g=float(sys.argv[8])
#
print " Elaboration with parameters: Distance=", Dist, "Graph=", Graph, " B=",B," f=",f," g=",g 
#
#take as input the files containing the coordinates of the cases and open it in read mode
#
c = open(c1, 'r')
#
# Create the lists of the coordinates of cases from a csv file
#
Sequential=[]
casesx=[]
casesy=[]
weight=[]
#
# read the csv file
#
with open(c1, 'rb') as fcsv:
  reader = csv.reader(fcsv)
  for row in reader:
   num, a, b, c= row
   Sequential.append(num)
   casesx.append(a)
   casesy.append(b)
   weight.append(c)
#
# ora le devo trasfromare in integer
# inoltre posso contare il numero di elementi presenti nel csv: questo metodo scambia inoltre righe con colonne
#
# test that X-coords and Y-coord are the same numbers
#
NB_CASES=len(casesx)
NB_CASESbis = len(casesy)
if NB_CASES != NB_CASESbis:
 print('The number of x coordinates is different from the number of y coordinates!! Please check the data')
#
#
#
Sequenza=[]
case_x=[]
case_y=[]
pesi=[]
#
# fill the lists of cases coordinates
#
for it in range (NB_CASES):
  case_x.append(float(casesx[it]))
  case_y.append(float(casesy[it]))
  pesi.append((float(weight[it])/100))  
#
# end for
#
im2 = Image.open(i2)
WIDTH, HEIGHT = im2.size
#
# tests that X-coords and Y-coords are NOT out of the image
#
uscita = 0
i=0
for i in range (NB_CASES):	
#
# tests points are not near 3 pixel to the border of the image
#
  if (case_x[i] > (WIDTH-3)):
    uscita = 1
  if (case_y[i] > (HEIGHT-3)):
    uscita = 1
  if (case_x[i] < 3):
    uscita = 1
  if (case_y[i] < 3):
    uscita = 1
#
# end for
#
if (uscita > 0):
#
# if exists a input data error exit
#
  print ""
  print " one or more points are OUT of the image OR are too near the image border (less 3 pixels)- check input data - exiting!"
  print ""
  sys.exit (0)
#
# end if
#
# if input data are correct execute te reconstruction 
#
# create an empty white image
#
i1=Image.new("RGB",(WIDTH,HEIGHT), "white")
i1.save("Imgwhite.bmp")
i1 = "Imgwhite.bmp"
#
# create the final image containing exclusively the results of elaboration
#
i4 = RES_PREFIX+"def.bmp"
im1 = Image.open(i1)
im1.save(i4)
#
# defines a zeros filled matrix with the same size of the image 
#
immagine= zeros( (WIDTH,HEIGHT) )
print "Data File",c1,"Image dimensions in pixel:",WIDTH,HEIGHT
#------
# create a file with the same size of im1 
# it will be bodified durig the reconstruction
# and it will contains the final elaboration
#
im4 = Image.open(i4)
#
# here begins the cycles of the rossmo function
if (Dist == "M"):
#---versione manhattan distance --------------------------------------------------------------------------------------
 for i in range (WIDTH):
  for j in range (HEIGHT):
   result = 0
   for n in range (NB_CASES):
#    manhattan distance 
     distance = (math.fabs(i - case_x[n])+math.fabs(j-case_y[n]))
     if distance > B:
       term1 = 1 / math.pow(distance,f)
       result = result + pesi[n]*term1
     else:
       term2=(math.pow(B,(g-f)))/math.pow((2*B-distance),g)
       result = result + pesi[n]*term2
#
# memorizzo il risultato in una matrice
#
   immagine[i][j]=result
# 
# it finds the max an the min value of the image
#
   if(i==0 and j==0):
     biggest_p = result;
     smallest_p = result;
   if(result > biggest_p):
     biggest_p = result; 
   if(smallest_p > result):
     smallest_p = result;
#-----End Manhattan---------------------------------------------------------------------------------------------------------
elif (Dist == "P"):
#------ power distance -------------------------------------------------------------------------------------------
#    teorema pitagora: math.sqrt(math.pow((math.fabs(i - case_x[n])), 2) + math.pow((math.fabs(j-case_y[n])),2))
#    the first term  is the x catete; the other the y catete
#---------------------------------------------------------------------------------------------------------------------
#
 for i in range (WIDTH):
  for j in range (HEIGHT):
   result = 0
   for n in range (NB_CASES):
#    Prower distance 
     esponente=1.0/3.0
     distance = math.pow(math.pow((math.fabs(i - case_x[n])), 3) + math.pow((math.fabs(j-case_y[n])),3),esponente)
     if distance > B:
       term1 = 1 / math.pow(distance,f)
       result = result + pesi[n]*term1
     else:
       term2=(math.pow(B,(g-f)))/math.pow((2*B-distance),g)
       result = result + pesi[n]*term2
#
# memorizzo il risultato in una matrice
#
   immagine[i][j]=result
# 
# it finds the max an the min value of the image
#
   if(i==0 and j==0):
     biggest_p = result;
     smallest_p = result;
   if(result > biggest_p):
     biggest_p = result; 
   if(smallest_p > result):
     smallest_p = result;

else:
#------ euclidean distance -------------------------------------------------------------------------------------------
#    teorema pitagora: math.sqrt(math.pow((math.fabs(i - case_x[n])), 2) + math.pow((math.fabs(j-case_y[n])),2))
#    the first term  is the x catete; the other the y catete
#---------------------------------------------------------------------------------------------------------------------
#
 for i in range (WIDTH):
  for j in range (HEIGHT):
   result = 0
   for n in range (NB_CASES):
#    euclidean distance 
     distance = math.sqrt(math.pow((math.fabs(i - case_x[n])), 2) + math.pow((math.fabs(j-case_y[n])),2))
     if distance > B:
       term1 = 1 / math.pow(distance,f)
       result = result + pesi[n]*term1
     else:
       term2=(math.pow(B,(g-f)))/math.pow((2*B-distance),g)
       result = result + pesi[n]*term2
#
# memorizzo il risultato in una matrice
#
   immagine[i][j]=result
# 
# it finds the max an the min value of the image
#
   if(i==0 and j==0):
     biggest_p = result;
     smallest_p = result;
   if(result > biggest_p):
     biggest_p = result; 
   if(smallest_p > result):
     smallest_p = result;
# 
#---------------------------------------------------------------------------------------------------------------------
# hence i must use biggest_p and smallest_p to standardize the value of result: smallest_p=0; biggest_P=>255 
#
# retrieves the result from the matrix and normalizes it
print " End of Calculus.. ",
print " Normalizzation and graphing.. ",
for i in range (WIDTH):
  for j in range (HEIGHT):
   result = immagine[i][j]
   resultrel = ((result-smallest_p)*255/(biggest_p-smallest_p))
#---- end for normalizzazione ------
#
# second part to insert colors that is yellow for low probability and red for high probability
#
# DA MODIFICARE in questo modo *ugo* : 
# massima probabilita' (>95%)= rosso (255,0,0)
# fra il 90 ed il 95        = arancio o rosa o qualche sfumatura del genere
# fra 85 e 90               = giallo
# fra 75 ed 80              = verde
# sotto 80                  = azzurro (sfumato)
#
   fracres, intres = math.modf(resultrel) 
#
# the above is the integer part of result, that is intres that i will use underneath
# i must force the double variable to become integer: let's see how it is
   intintres = int(intres)
#
# nuova versione di put pixel - si comincia dal valore di pixel piu basso perche piu frequente.
#
   if (Graph=="B"):
    if (intintres <= 219):
     # disegna il pixel in toni di azzurro
     im4.putpixel((i,j),grigio)
    elif ((intintres > 219)and (intintres <= 231)):
     # disegna il pixel in verde
     im4.putpixel((i,j),verde)
    elif ((intintres>231)and(intintres <=243)):
     # disegna il pixel in giallo
     im4.putpixel((i,j),giallo) 
    else:
     # if (intintres > 243):
     # disegna il pixel in rosso
     im4.putpixel((i,j),rosso)
    # endif
   else:
    if (intintres <= 219):
     # disegna il pixel in toni di azzurro
     im4.putpixel((i,j),(0, intintres, 255-intintres))
    elif ((intintres > 219)and (intintres <= 231)):
     # disegna il pixel in verde
     im4.putpixel((i,j),(0, 255, 0))
    elif ((intintres>231)and(intintres <=243)):
     # disegna il pixel in giallo
     im4.putpixel((i,j),giallo) 
    else:
     # if (intintres > 243):
     # disegna il pixel in rosso
     im4.putpixel((i,j),rosso)
    # endif
   #endif
#
#
# End FOR (doppio) -----
#
# it puts a small cross on the cases coordinates
# rifare quest aparte di codice mettendo if case_x[cross]< (WIDTH-3): per primo
# poi elif per passare al caso successivo: so that i controlli successivi a -2 e -1 li fa only if the first condition is not satisfied (more common
# option) may be i have to insert a return before the successive elif but probably not
# successive step to produce a function that draws a cross in that position x,y 
#
# Plots a small white circle in the point of interest
#
print" Drawing points.. ",
draw = ImageDraw.Draw(im4)
for cross in range (NB_CASES):
  draw.ellipse((case_x[cross]-1, case_y[cross]-1, case_x[cross]+1, case_y[cross]+1), fill=(255,255,255))
#
# disegna le etichette solo se richiesto
#
  if (Graph <> "B"):
   if (Graph <> "N"):
    testo="n."+str(int(Sequential[cross]))
    #testo="n."+str(int(cross+1))
    #
    # disegna le etichette ed anche i pesi
    #
    if (Graph <> "O"):
       testo=testo +" ("+str(weight[cross])+"%)"
    lung_testo=len(testo)
    draw.text((case_x[cross]-int(5*lung_testo/2), case_y[cross]-13),testo,fill=(255,255,0))
#
# with the above instruction it says that 255-result is not an integer but a double and i agree
# may be i should normalize tha value but in the c instruction ai don't see anything like that
# im4.putpixel((i,j),(255, 255, 0)) if i use this instruction i obtain an image that is completely yellow
#
print" Saving image and deleting temporary files"
im4.save(i4)
im1=Image.open(i2)
im2=Image.open(i4)
im3=Image.blend(im1, im2, BLEND_PERCENTAGE)
im3.save(final_image)
#
# delete temporary file
#
os.remove(i1)
#
# non va cancellata la IM4 *ugo*
#
#uso blend the value of the mask of 0.8 apparently works well,, It can be changed
#
print" "
print" --------------------------------- "
print" "
print(' Resulting image saved with the indicated name - end of execution')
print" "
print" --------------------------------- "
#
# END PROGRAM
#
print" "


