#!/usr/bin/python
#
# need to implement command line input
#
import os, sys, csv, string
import Image
#-------------
#jack takes as input the dataset in csv (check the format since it contains an order number and a weight
# set to 100 as default), then the number of replicates and the number of items to be deleted at each replicate
#
# Filename - to be manually changed with other case
#
i2csv="ctaxipesi.csv"
replicates=100
itemstodelete=4
MAP_NAME="Mediterranean.bmp"
#-----------------
ESEGUI=True

comando = "python Jack0_3.py "+str(i2csv)+" "+str(replicates)+" "+str(itemstodelete)
#jack produces the jackknifed data sets, one data set in csv for each replicate

print comando
print " "
if ESEGUI:
 os.system(comando)


for i in range (replicates):
#for each data set i perform a geoprofiling
 nome="replica"+str(i+1)+"di"+str(replicates)+"tolti"+str(itemstodelete)+"in"+str(i2csv)+".txt"
 replicate="rep"+str(i+1)+"di"+str(replicates)
 comando2= "python Geoprof2_0_3_csv.py "+str(MAP_NAME)+" "+str(nome)+" "+str(replicate)+" E O 13 0.4 0.4"
 if ESEGUI:
  os.system(comando2)

#input Geprof is: the map, csv data, prefix of the resulting images; the last value, here O, is about the graph format
#---------
#miximage



comando3 = "python miximage11.py"+" "+"def"+" "+str(replicates)+" "+" ANDimageoutput.bmp"+" AND" 

print comando3
print " "
if ESEGUI:
 os.system(comando3)
i1=Image.open("ANDimageoutput.bmp")
i2=Image.open(MAP_NAME)
ima=Image.blend(i1,i2,0.5)
ima.save("ANDmapped.bmp")


comando3 = "python miximage11.py"+" "+"def"+" "+str(replicates)+" "+" ORimageoutput.bmp"+" OR "

print comando3
print " "
if ESEGUI:
 os.system(comando3)
i3=Image.open("ORimageoutput.bmp")
ima2=Image.blend(i3,i2,0.5)
ima2.save("ORmapped.bmp")


comando3 = "python miximage11.py"+" "+"def"+" "+str(replicates)+" "+" MEANimageoutput.bmp"+" MEAN "

print comando3
print " "
if ESEGUI:
 os.system(comando3)
i4=Image.open("MEANimageoutput.bmp")
ima3=Image.blend(i4,i2,0.5)
ima3.save("MEANmapped.bmp")


#
print "\nfine"

